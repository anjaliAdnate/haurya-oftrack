import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ToastController, AlertController } from 'ionic-angular';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import * as moment from 'moment';
declare var RazorpayCheckout: any;

@IonicPage()
@Component({
  selector: 'page-paytmwalletlogin',
  templateUrl: 'paytmwalletlogin.html',
})
export class PaytmwalletloginPage {
  paytmmail: any;
  paytmnumber: any;
  paytmotp: number;
  successresponse: boolean = false;
  inputform: boolean = false;
  razor_key: string = 'rzp_live_eZdWnBRmYRCLWo';
  paymentAmount: number = 150000;
  currency: any = 'INR';
  parameters: any;
  islogin: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public apiCall: ApiServiceProvider,
    private menu: MenuController,
    public toast: ToastController,
    private alertCtrl: AlertController
  ) {
    // this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    // console.log("islogin devices => " + JSON.stringify(this.islogin));
    this.islogin = localStorage.getItem('details') ? JSON.parse(localStorage.getItem('details')) : "";
    this.paytmnumber = this.islogin.phn;
    this.parameters = navParams.get('param');
    console.log("parameters", this.parameters);
    if (localStorage.getItem('default_curreny') !== null) {
      this.currency = localStorage.getItem('default_curreny');
    }
  }
  ionViewDidEnter() {

    this.menu.enable(true);
  }

  paytmSignupOTPResponse: any;

  payWithRazor() {
    var orderId;
    // debugger
    var options = {
      description: 'Credits towards consultation',
      image: 'https://i.imgur.com/GO0jiDP.jpg',
      currency: this.currency, // your 3 letter currency code
      key: this.razor_key, // your Key Id from Razorpay dashboard
      amount: this.paymentAmount, // Payment amount in smallest denomiation e.g. cents for USD
      name: this.parameters.Device_Name,
      // order_id: '123dknkdslnflk',
      prefill: {
        email: this.islogin.email,
        contact: this.paytmnumber,
        name: this.islogin.fn + ' ' + this.islogin.ln
      },
      theme: {
        color: '#ef4136'
      },
      modal: {
        ondismiss: function () {
          console.log('dismissed')
        }
      }
    };
    debugger
    let that = this;
    var successCallback = function (success) {
      // alert('order_id: ' + JSON.stringify(success));
      that.showAlert(success.razorpay_payment_id);
      orderId = success.razorpay_order_id
      var signature = success.razorpay_signature
    }

    var cancelCallback = function (error) {
      console.log("razorpay error: ", error)
      that.onFailed(error);
      // alert(error.description + ' (Error ' + error.code + ')')
    }

    // RazorpayCheckout.open(options, this.successCallback(), cancelCallback);
    RazorpayCheckout.on('payment.success', successCallback)
    RazorpayCheckout.on('payment.cancel', cancelCallback)
    RazorpayCheckout.open(options)
  }

  showAlert(id) {
    let alert = this.alertCtrl.create({
      message: "Payment successfull.. payment Id - " + id,
      buttons: [{
        text: 'Okay',
        handler: () => {
          this.updateExpDate();
        }
      }]
    });
    alert.present();
  }

  onFailed(err) {
    console.log(err);
    let alert = this.alertCtrl.create({
      message: 'Transaction Failed! Please try again with valid credentials',
      buttons: [{
        text: 'Try Again',
        handler:() => {
          this.goBack();
        }
      }]
    });
    alert.present();
  }

  goBack() {
    this.navCtrl.pop();
  }

  updateExpDate() {
    debugger
    var tempdate = new Date();
    tempdate.setDate(tempdate.getDate() + 365);
    var expDate = moment(new Date(tempdate), 'DD-MM-YYYY').format("YYYY-MM-DD");
    console.log("updated expiry date: ", expDate);
    var url = this.apiCall.mainUrl + 'devices/editDevMaster';
    var payload = {
      // _id: this.parameters.Device_ID,
      _id: this.parameters._id,
      expiration_date: new Date(expDate).toISOString()
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(url, payload)
      .subscribe(respData => {
        this.apiCall.stopLoading();
        console.log("respData of expiry date updation: ", respData);
        let toast = this.toastCtrl.create({
          message: 'Payment success!!',
          duration: 1000,
          position: 'middle'
        });
        toast.onDidDismiss(() => {
          this.navCtrl.pop();
        });
        toast.present();

      },
        err => {
          console.log("oops got error: ", err);
          this.apiCall.stopLoading();

        });
  }

}
