import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';
import { SocialSharing } from '@ionic-native/social-sharing';
import { File } from '@ionic-native/file';
import * as XLSX from 'xlsx';
import { b } from '@angular/core/src/render3';
import { OuterSubscriber } from 'rxjs/OuterSubscriber';
/**
 * Generated class for the WorkingHoursReportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-working-hours-report',
  templateUrl: 'working-hours-report.html',
})
export class WorkingHoursReportPage {

  islogin: any;
  Ignitiondevice_id: any = [];
  igiReport: any[] = [];

  datetimeEnd: any;
  datetimeStart: string;
  devices: any;
  isdevice: string;
  portstemp: any;
  datetime: any;
  igiReportData: any[] = [];
  daywiseReport: any = [];
  a: number;
  b: number;
  c: number;
  value: number;
  TotalWorkingHours: number = 0;
  device_id: any = []
  locationEndAddress: any;
  selectedVehicle: any;
  twoMonthsLater: any = moment().subtract(2, 'month').format("YYYY-MM-DD");
  today: any = moment().format("YYYY-MM-DD");

  constructor(
    public file: File,
    public socialSharing: SocialSharing,
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicalldaywise: ApiServiceProvider,
    public toastCtrl: ToastController,
    private geocoderApi: GeocoderProvider) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);

    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
  }

  ngOnInit() {
    this.getdevices();
  }

  getdevices() {
    var baseURLp = this.apicalldaywise.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicalldaywise.startLoading().present();
    this.apicalldaywise.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicalldaywise.stopLoading();
        this.devices = data;
        this.portstemp = data.devices;
      },
        err => {
          this.apicalldaywise.stopLoading();
          console.log(err);
        });
  }
  OnExport = function () {
    let sheet = XLSX.utils.json_to_sheet(this.igiReportData);
    let wb = {
      SheetNames: ["export"],
      Sheets: {
        "export": sheet
      }
    };

    let wbout = XLSX.write(wb, {
      bookType: 'xlsx',
      bookSST: false,
      type: 'binary'
    });

    function s2ab(s) {
      let buf = new ArrayBuffer(s.length);
      let view = new Uint8Array(buf);
      for (let i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    }

    let blob = new Blob([s2ab(wbout)], { type: 'application/octet-stream' });
    let self = this;

    // this.getStoragePath().then(function (url) {
    self.file.writeFile(self.file.dataDirectory, "ignition_report_download.xlsx", blob, { replace: true })
      .then((stuff) => {
        // alert("file downloaded at: " + self.file.dataDirectory);
        if (stuff != null) {
          // self.socialSharing.share('CSV file export', 'CSV export', url, '')
          self.socialSharing.share('CSV file export', 'CSV export', stuff['nativeURL'], '')
        } else return Promise.reject('write file')
      }).catch(() => {
        alert("error creating file at :" + self.file.dataDirectory);
      });
    // });
  }

  changeDate(key) {
    this.datetimeStart = undefined;
    if (key === 'today') {
      this.datetimeStart = moment({ hours: 0 }).format();
    } else if (key === 'yest') {
      this.datetimeStart = moment().subtract(1, 'days').format();
    } else if (key === 'week') {
      this.datetimeStart = moment().subtract(1, 'weeks').endOf('isoWeek').format();
    } else if (key === 'month') {
      this.datetimeStart = moment().startOf('month').format();
    }
  }

  getDaywisedevice(selectedVehicle) {
    console.log("inside")
    this.device_id = [];
    if (selectedVehicle.length > 0) {
      if (selectedVehicle.length > 1) {
        for (var t = 0; t < selectedVehicle.length; t++) {
          this.device_id.push(selectedVehicle[t].Device_ID)
        }
      } else {
        this.device_id.push(selectedVehicle[0].Device_ID)
      }
    } else return;
    console.log("selectedVehicle=> ", this.device_id)
  }

  daywiseReportData = [];
  getDaywiseReport() {
    let that = this;
    this.daywiseReport = []
    this.daywiseReportData = [];

    if (this.device_id == undefined) {
      this.device_id = "";
    }
    this.apicalldaywise.startLoading().present();
    this.apicalldaywise.getDaywiseReportApi(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), this.islogin._id, this.device_id)
      .subscribe(data => {
        this.apicalldaywise.stopLoading();
        // this.summaryReport = data;
        console.log("cheack daywise data", data);
       //console.log("cheack daywise data", data[i]['Moving Time']);
        this.daywiseReport = data
        console.log("recheack", this.daywiseReport);
        if (this.daywiseReport.length > 0) {
        this.innerFunc(this.daywiseReport);
        } else {
          let toast = this.toastCtrl.create({
            message: 'Report(s) not found for selected dates/vehicle.',
            duration: 1500,
            position: 'bottom'
          })
          toast.present();
        }
        // console.log("again cheack", this.innerFunc);
      }, error => {
        this.apicalldaywise.stopLoading();
        console.log(error);
      })
  }

  innerFunc(daywiseReport) {
    debugger
    let outerthis = this;
    var i = 0, howManyTimes = daywiseReport.length;
    function f() {
      // console.log("conversion: ", Number(outerthis.summaryReport[i].devObj[0].Mileage))
      // var hourconversion = 2.7777778 / 10000000;
      outerthis.daywiseReportData.push(
        {
          // 'Date': new Date(daywiseReport[i].Date),
          'Date': moment(daywiseReport[i].Date).format('ddd MMM DD YYYY'),
          'VRN': daywiseReport[i].VRN,
          'end_location': daywiseReport[i].end_location,
          'start_location': daywiseReport[i].start_location,
          'Distance(Kms)': daywiseReport[i]["Distance(Kms)"],
          'Moving Time': daywiseReport[i]['Moving Time'],
          'Stoppage Time': daywiseReport[i]['Stoppage Time'],
          'Idle Time': daywiseReport[i]['Idle Time'],
          //'AddedValue': daywiseReport[i]['c']
        });
        console.log("moving time", outerthis.daywiseReport[i]['Moving Time']); 
        var a = outerthis.daywiseReport[i]['Moving Time'];
        var b = outerthis.daywiseReport[i]['Idle Time']
        var c = a + b;
        console.log("added time", c); 
        outerthis.daywiseReportData[i].AddedValue = c;
        var d = outerthis.daywiseReport[i]['Stoppage Time'];
        console.log("stoppage time", d);
        var e = d - c;
        console.log("duration", e);
        outerthis.daywiseReportData[i].WorkingDuration = e;
        outerthis.TotalWorkingHours += outerthis.daywiseReportData[i].WorkingDuration;
        console.log("total hours", outerthis.TotalWorkingHours);
        // outerthis.start_address(daywiseReport[i], i);
        // outerthis.end_address(daywiseReport[i], i);    
      console.log("see data here: ", outerthis.daywiseReportData)
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    }
    f();
  }

  millisecondConversion(duration) {
    var minutes = Math.floor((duration / (1000 * 60)) % 60);
    var hours = Math.floor((duration / (1000 * 60 * 60)) % 24);
    hours = (hours < 10) ? 0 + hours : hours;
    minutes = (minutes < 10) ? 0 + minutes : minutes;
    return hours + ":" + minutes;
  }

  start_address(item, index) {
    let that = this;
    if (!item.start_location) {
      that.daywiseReportData[index].StartLocation = "N/A";
      return;
    }
    let tempcord = {
      "lat": item.start_location.lat,
      "long": item.start_location.long
    }
    this.apicalldaywise.getAddress(tempcord)
      .subscribe(res => {
        if (res.message == "Address not found in databse") {
          this.geocoderApi.reverseGeocode(item.start_location.lat, item.start_location.long)
            .then(res => {
              var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
              that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
              that.daywiseReportData[index].StartLocation = str;
              // console.log("inside", that.address);
            })
        } else {
          that.daywiseReportData[index].StartLocation = res.address;
        }
      })
  }


  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apicalldaywise.saveGoogleAddressAPI(payLoad)
      .subscribe(respData => {
        console.log("check if address is stored in db or not? ", respData)
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }



  timeToMins(time) {
    var b = time.split(':');
    return b[0]*60 + +b[1];
  }
  
  // Convert minutes to a time in format hh:mm
  // Returned value is in range 00  to 24 hrs
  timeFromMins(mins) {
    function z(n){return (n<10? '0':'') + n;}
    var h = (mins/60 |0) % 24;
    var m = mins % 60;
    return z(h) + ':' + z(m);
  }
  
  // Add two times in hh:mm format
    addTimes(a, b) {
    return this.timeFromMins(this.timeToMins(this.a) + this.timeToMins(this.b));
  }
  
  


  // end_address(item, index) {
  //   let that = this;
  //   if (!item.end_location) {
  //     that.daywiseReportData[index].EndLocation = "N/A";
  //     return;
  //   }
  //   let tempcord = {
  //     "lat": item.end_location.lat,
  //     "long": item.end_location.long
  //   }
  //   this.apicalldaywise.getAddress(tempcord)
  //     .subscribe(res => {
  //       console.log("test");
  //       console.log("result", res);
  //       console.log(res.address);
  //       // that.allDevices[index].address = res.address;
  //       if (res.message == "Address not found in databse") {
  //         this.geocoderApi.reverseGeocode(item.end_location.lat, item.end_location.long)
  //           .then(res => {
  //             var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
  //             that.saveAddressToServer(str, item.end_location.lat, item.end_location.long);
  //             that.daywiseReportData[index].EndLocation = str;
  //             // console.log("inside", that.address);
  //           })
  //       } else {
  //         that.daywiseReportData[index].EndLocation = res.address;
  //       }
  //     })
  // }
}
