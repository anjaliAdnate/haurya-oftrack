import { Component } from "@angular/core";
import { IonicPage, NavParams, ViewController, ToastController, NavController } from "ionic-angular";

@IonicPage()
@Component({
    selector: 'page-expired',
    templateUrl: './expired.html'
})

export class ExpiredPage {
    data: any;

    constructor(
        public params: NavParams,
        public viewCtrl: ViewController,
        public toastCtrl: ToastController,
        public navCtrl: NavController
    ) {
        if (params.get("param")) {
            this.data = params.get("param");
        }
    }

    activate() {
        // this.navCtrl.push("AddDevicesPage");
        this.navCtrl.push("PaytmwalletloginPage", {
            "param": this.data
        })
        console.log("Redirect to paytm page")
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }


}