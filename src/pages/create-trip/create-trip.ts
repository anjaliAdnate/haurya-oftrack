import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Events } from 'ionic-angular';
import { GoogleMaps, CameraPosition, ILatLng, LatLng, GoogleMap, GoogleMapsEvent, LatLngBounds } from '@ionic-native/google-maps';
import { NativeGeocoder, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
// import { Geolocation } from '@ionic-native/geolocation';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { Storage } from '@ionic/storage';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';

declare var google: any;

@IonicPage()
@Component({
  selector: 'page-create-trip',
  templateUrl: 'create-trip.html',
})
export class CreateTripPage implements OnInit, OnDestroy {
  acService: any;
  autocompleteItems: any = [];
  autocomplete: any = {};
  newLat: any = 0;
  newLng: any = 0;
  map: GoogleMap;
  mapElement: HTMLElement;
  storedLatLng: any;
  tripData: any = {};
  deviceDetails: any = {};
  userdetails: any;
  yourLoc: string;
  service = new google.maps.DistanceMatrixService();
  _commonVar: any = {};
  _id: any;
  expectation: any = {};
  showBtn: boolean = false;
  tripName: any;

  constructor(
    public toastCtrl: ToastController,
    // public geoLocation: Geolocation,
    public apiCall: ApiServiceProvider,
    public nativeGeocoder: NativeGeocoder,
    public event: Events,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    private geocoderApi: GeocoderProvider
  ) {
    console.log("Param data: ", navParams.get("paramData"))
    console.log("trip data: ", navParams.get("tripData"))
    this.deviceDetails = navParams.get("paramData");
    this.tripData = navParams.get("tripData");
    this.acService = new google.maps.places.AutocompleteService();
    this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> " + JSON.stringify(this.userdetails));
  }

  getAddress(coordinates) {
    let that = this;
    if (!coordinates) {
      that.autocomplete.yourLocation = 'N/A';
      return;
    }
    let tempcord = {
      "lat": coordinates.lat,
      "long": coordinates.long
    }
    this.apiCall.getAddress(tempcord)
      .subscribe(res => {
        console.log("test");
        console.log("result", res);
        if (res.message == "Address not found in databse") {
          this.geocoderApi.reverseGeocode(coordinates.lat, coordinates.long)
            .then(res => {
              var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
              that.saveAddressToServer(str, coordinates.lat, coordinates.long);
              that.autocomplete.yourLocation = str;
              // console.log("inside", that.address);
            })
        } else {
          that.autocomplete.yourLocation = res.address;
        }
        // console.log(that.address);
      });
  }

  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apiCall.saveGoogleAddressAPI(payLoad)
      .subscribe(respData => {
        console.log("check if address is stored in db or not? ", respData)
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }

  ionViewDidEnter() {
    this.autocompleteItems = [];
    this.autocomplete = {
      query: '',
      yourLocation: 'N/A'
    };
    // debugger
    if (this.deviceDetails.last_location != undefined) {
      this.drawGeofence(this.deviceDetails.last_location['lat'], this.deviceDetails.last_location['long']);

      let that = this;
      var payload = {
        "lat": this.deviceDetails.last_location['lat'],
        "long": this.deviceDetails.last_location['long'],
        // "api_id": "1"
      }
      that.getAddress(payload);
      // this.apiCall.getAddressApi(payload)
      //   .subscribe((data) => {
      //     // console.log("got address: "+ data.results)
      //     if(data.results[2] != undefined || data.results[0] != null) {
      //       that.autocomplete.yourLocation = data.results[0].formatted_address;
      //     } else {
      //       that.autocomplete.yourLocation = 'N/A';
      //     }
      //   })
    }

  }

  ngOnInit() {
    // this.autocompleteItems = [];
    // this.autocomplete = {
    //   query: '',
    //   yourLocation: 'N/A'
    // };
    // // debugger
    // if (this.deviceDetails.last_location != undefined) {
    //   this.drawGeofence(this.deviceDetails.last_location['lat'], this.deviceDetails.last_location['long']);

    //   let that = this;
    //   var payload = {
    //     "lat": this.deviceDetails.last_location['lat'],
    //     "long": this.deviceDetails.last_location['long'],
    //     // "api_id": "1"
    //   }
    //   that.getAddress(payload);
    //   // this.apiCall.getAddressApi(payload)
    //   //   .subscribe((data) => {
    //   //     // console.log("got address: "+ data.results)
    //   //     if(data.results[2] != undefined || data.results[0] != null) {
    //   //       that.autocomplete.yourLocation = data.results[0].formatted_address;
    //   //     } else {
    //   //       that.autocomplete.yourLocation = 'N/A';
    //   //     }
    //   //   })
    // }

  }

  ngOnDestroy() {
    if (localStorage.getItem("travelDetailsObject") != null) {
      localStorage.removeItem("travelDetailsObject");
    }
  }
  updateSearch() {
    // debugger
    console.log('modal > updateSearch');
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    let that = this;
    let config = {
      //types:  ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'
      input: that.autocomplete.query,
      componentRestrictions: {}
    }
    this.acService.getPlacePredictions(config, function (predictions, status) {
      console.log('modal > getPlacePredictions > status > ', status);
      console.log("lat long not find ", predictions);

      that.autocompleteItems = [];
      predictions.forEach(function (prediction) {
        that.autocompleteItems.push(prediction);
      });
      console.log("autocompleteItems=> " + that.autocompleteItems)
    });
  }

  chooseItem(item) {
    let that = this;
    that.autocomplete.query = item.description;
    console.log("console items=> " + JSON.stringify(item))
    that.autocompleteItems = [];

    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
    that.apiCall.startLoading().present();
    this.nativeGeocoder.forwardGeocode(item.description, options)
      .then((coordinates: NativeGeocoderForwardResult[]) => {
        that.apiCall.stopLoading();
        console.log('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude)
        that.newLat = coordinates[0].latitude;
        that.newLng = coordinates[0].longitude;

        var dest = new LatLng(parseFloat(that.newLat), parseFloat(that.newLng));
        var sources = new LatLng(that.deviceDetails.last_location.lat, that.deviceDetails.last_location.long);

        that.calcRoute(sources, dest);
        // this.map.addMarker({
        //   title: 'Source',
        //   icon: 'red',
        //   position: sources,
        // }).then((data) => {
        //   console.log("Marker added")
        // })
      })
      .catch((error: any) => {
        that.apiCall.stopLoading();
        console.log(error);
      }
      );
  }

  setDestination() {
    var url = this.apiCall.mainUrl + "user_trip/planTrip";
    
    if (this.tripName == undefined) {
      let toast = this.toastCtrl.create({
        message: 'Please enter the trip name.',
        duration: 1500,
        position: 'middle'
      })
      toast.present();
    } else {

      var payload = {
        "user": this.userdetails._id,
        "device": this.deviceDetails._id,
        "start_loc": {
          "lat": this.deviceDetails.last_location.lat,
          "long": this.deviceDetails.last_location.long
        },
        "trip_status": 'Started',
        "end_loc": {
          "lat": this.newLat,
          "long": this.newLng
        },
        "trip_name": this.tripName,
        "start_time": new Date().toISOString()
      }
      this.apiCall.startLoading().present();
      this.apiCall.urlpasseswithdata(url, payload)
        .subscribe(data => {
          this.apiCall.stopLoading();
          console.log("resceved data: ", data)

          let toast = this.toastCtrl.create({
            message: 'Trip has been created successfully.',
            duration: 1500,
            position: 'bottom'
          })
          toast.present();
          let that = this;
          if (data.message == 'Trip Created') {
            this.storage.set("TRIPDATA", data).then(res => {
              console.log("ionic storage res: ", res)
              that.event.publish("tripstatUpdated", data.message)
              that.navCtrl.pop();
            })
          }
        },
          err => {
            this.apiCall.stopLoading();
            let toast = this.toastCtrl.create({
              message: 'Something went wrong.. Please try after some time.',
              duration: 1500,
              position: 'bottom'
            })
            toast.present();
          })
    }
  }


  drawGeofence(lat, lng) {

    if (this.map != undefined) {
      this.map.remove();
    }

    this.mapElement = document.getElementById('mapTrip');
    console.log(this.mapElement);

    this.map = GoogleMaps.create(this.mapElement);

    // Wait the MAP_READY before using any methods.
    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
        console.log('Map is ready!');
        let pos: CameraPosition<ILatLng> = {
          target: new LatLng(lat, lng),
          zoom: 12,
          tilt: 30
        };
        this.map.moveCamera(pos);
        this.map.addMarker({
          title: 'Source',
          position: new LatLng(lat, lng),
          icon: 'red'
        }).then((data) => {
          console.log("Marker added")
          this.newLat = lat;
          this.newLng = lng;
        })
        // });
      });
  }

  calcRoute(start, end) {
    debugger
    this._commonVar.AIR_PORTS = [];
    var directionsService = new google.maps.DirectionsService();
    let that = this;
    var request = {
      origin: start,
      destination: end,
      // waypoints: waypts,
      optimizeWaypoints: true,
      travelMode: google.maps.TravelMode.DRIVING
    };

    directionsService.route(request, function (response, status) {

      if (status == google.maps.DirectionsStatus.OK) {
        var path = new google.maps.MVCArray();
        for (var i = 0, len = response.routes[0].overview_path.length; i < len; i++) {
          path.push(response.routes[0].overview_path[i]);
          // var k;
          if (path.j !== undefined) {
            that._commonVar.AIR_PORTS.push({
              lat: path.j[i].lat(), lng: path.j[i].lng()
            });
          } else {
            that._commonVar.AIR_PORTS.push({
              lat: path.g[i].lat(), lng: path.g[i].lng()
            });
          }


          if (that._commonVar.AIR_PORTS.length > 1) {

            that.map.addMarker({
              title: 'Destination',
              position: end,
              icon: 'green'
            })
            that.map.addPolyline({
              'points': that._commonVar.AIR_PORTS,
              'color': '#4aa9d5',
              'width': 4,
              'geodesic': true,
            }).then(() => {

              that.getTravelDetails(start, end);
              that.showBtn = true;
            })
          }
        }
        let bounds = new LatLngBounds(that._commonVar.AIR_PORTS);
        that.map.moveCamera({
          target: bounds
        })
        // that.apiCall.stopLoading();
        // that.socketInit(that._commonVar._data);
      }
    });
  }

  getTravelDetails(source, dest) {
    let that = this;
    this._id = setInterval(() => {
      if (localStorage.getItem("travelDetailsObject") != null) {
        if (that.expectation.distance == undefined && that.expectation.duration == undefined) {
          // if (that.expectation == undefined) {
          that.expectation = JSON.parse(localStorage.getItem("travelDetailsObject"));
          console.log("expectation: ", that.expectation)
        } else {
          clearInterval(this._id);
        }
      }
    }, 3000)
    that.service.getDistanceMatrix({
      origins: [source],
      destinations: [dest],
      travelMode: 'DRIVING'
    }, that.callback);
  }

  callback(response, status) {
    let travelDetailsObject;
    if (status == 'OK') {
      var origins = response.originAddresses;
      for (var i = 0; i < origins.length; i++) {
        var results = response.rows[i].elements;
        for (var j = 0; j < results.length; j++) {
          var element = results[j];
          var distance = element.distance.text;
          var duration = element.duration.text;
          travelDetailsObject = {
            distance: distance,
            duration: duration
          }
        }
      }
      localStorage.setItem("travelDetailsObject", JSON.stringify(travelDetailsObject));
    }
  }


}
